package com.cognizant;

import com.cognizant.challenge.ChallengeRepository;
import com.cognizant.challenge.ChallengeRequest;
import com.cognizant.challenge.ChallengeResponse;
import com.cognizant.challenge.ChallengeService;
import com.cognizant.jdoodle.JDoodleService;
import com.cognizant.task.Task;
import com.cognizant.task.TaskService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.doReturn;

@ExtendWith(MockitoExtension.class)
public class ChallengeServiceTest {

    private ChallengeService challengeService;
    @Mock
    private TaskService taskService;
    @Mock
    private ChallengeRepository challengeRepository;
    @Mock
    private JDoodleService jDoodleService;

    @BeforeEach
    public void setup() {
        challengeService = new ChallengeService(taskService, challengeRepository, jDoodleService);
    }

    @Test
    public void givenChallengeRequest_shouldReturnTrue() {
        // given
        String output = "output";
        Task task = Task.builder()
                .taskId(1)
                .name("taskName")
                .input("input")
                .output(
                        output)
                .build();

        ChallengeRequest request = ChallengeRequest.builder()
                .name("name1")
                .taskId(1)
                .script("script")
                .build();

        doReturn(task).when(taskService).findById(1);
        doReturn(output).when(jDoodleService).execute(request);


        // when
        ChallengeResponse response = challengeService.solve(request);

        //then
        assertThat(response)
                .isNotNull()
                .hasFieldOrPropertyWithValue("result", true);
    }

    @Test
    public void givenChallengeRequest_shouldReturnFalse() {
        // given
        Task task = Task.builder()
                .taskId(1)
                .name("taskName")
                .input("input")
                .output("output1")
                .build();

        ChallengeRequest request = ChallengeRequest.builder()
                .name("name1")
                .taskId(1)
                .script("script")
                .build();

        doReturn(task).when(taskService).findById(1);
        doReturn("output2").when(jDoodleService).execute(request);


        // when
        ChallengeResponse response = challengeService.solve(request);

        //then
        assertThat(response)
                .isNotNull()
                .hasFieldOrPropertyWithValue("result", false);
    }

}
