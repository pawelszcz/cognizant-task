insert into task (task_id, name, description, input, output) values (1, 'sum', 'sum of 2 numbers, script ex: let sum = parseInt(process.argv[2]) + parseInt(process.argv[3] ); console.log(sum) ', '7 9', '16');
insert into task (task_id, name, description, input, output) values (2, 'multiply', 'multiply 2 numbers, script ex: let sum = parseInt(process.argv[2]) * parseInt(process.argv[3] ); console.log(sum)', '4 5', '20');
insert into task (task_id, name, description, input, output) values (3, 'abs', 'absolute value of number', '-56', '56');
insert into task (task_id, name, description, input, output) values (4, 'power', '2 to n power', '3', '8');
