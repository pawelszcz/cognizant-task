create table task (
     task_id        integer not null,
     name           varchar(50) not null,
     description    varchar(200),
     input          varchar(50) not null,
     output         varchar(50) not null,
     primary key (task_id)
);

create table challenge
(
    name            varchar(50) not null,
    no_of_success     integer not null,
    no_of_tasks       integer not null,
    primary key (name)
);
