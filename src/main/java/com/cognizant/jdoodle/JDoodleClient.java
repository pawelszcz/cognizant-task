package com.cognizant.jdoodle;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(value = "JDoodleClient", url = "https://api.jdoodle.com")
public interface JDoodleClient {

    @PostMapping("/execute")
    JDoodleResponse execute(@RequestBody JDoodleRequest request);
}
