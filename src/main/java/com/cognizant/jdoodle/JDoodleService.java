package com.cognizant.jdoodle;

import com.cognizant.challenge.ChallengeRequest;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class JDoodleService {

    private JDoodleClient jDoodleClient;

    @Value("${jdoodle.clientId}")
    private String clientId;
    @Value("${jdoodle.clientSecret}")
    private String clientSecret;
    @Value("${jdoodle.language}")
    private String language;
    @Value("${jdoodle.versionIndex}")
    private String versionIndex;

    public JDoodleService(JDoodleClient jDoodleClient) {
        this.jDoodleClient = jDoodleClient;
    }

    public String execute(ChallengeRequest challengeRequest) {
        JDoodleRequest request = JDoodleRequest.builder()
                .clientId(clientId)
                .clientSecret(clientSecret)
                .language(language)
                .versionIndex(versionIndex)
                .script(challengeRequest.getScript())
                .args(challengeRequest.getInput())
                .build();
        JDoodleResponse response = jDoodleClient.execute(request);

        return trim(response.getOutput());
    }

    private String trim(String output) {
        return output.substring(0, output.indexOf("\n"));
    }
}
