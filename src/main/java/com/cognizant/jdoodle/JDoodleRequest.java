package com.cognizant.jdoodle;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class JDoodleRequest{
    private String args;
    private String script;
    private String clientId;
    private String clientSecret;
    private String language;
    private String versionIndex;


}
