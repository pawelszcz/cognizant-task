package com.cognizant.challenge;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ChallengeRepository extends JpaRepository<Challenge, String> {

    @Query("SELECT c FROM Challenge c order by c.noOfSuccess desc")
    List<Challenge> findAllSortedBySuccess(Pageable pageable);
}
