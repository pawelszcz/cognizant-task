package com.cognizant.challenge;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/challenge")
@AllArgsConstructor
public class ChallengeController {

    private ChallengeService challengeService;

    @PostMapping
    public ChallengeResponse solve(@RequestBody ChallengeRequest request) {
        return challengeService.solve(request);
    }

    @GetMapping
    public List<Challenge> top3() {
        return challengeService.getTop3Results();
    }

}
