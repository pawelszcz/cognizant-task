package com.cognizant.challenge;


import com.cognizant.jdoodle.JDoodleService;
import com.cognizant.task.Task;
import com.cognizant.task.TaskService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class ChallengeService {

    private TaskService taskService;
    private ChallengeRepository challengeRepository;
    private JDoodleService jDoodleService;

    public ChallengeResponse solve(ChallengeRequest challengeRequest) {
        Task task = taskService.findById(challengeRequest.getTaskId());
        challengeRequest.setInput(task.getInput());
        String output = jDoodleService.execute(challengeRequest);
        boolean result = task.getOutput().equals(output);

        Challenge challenge = challengeRepository.findById(challengeRequest.getName()).orElse(new Challenge(challengeRequest.getName(), 0, 0));
        if (result)
            challenge.setNoOfSuccess(challenge.getNoOfSuccess()+1);
        challenge.setNoOfTasks(challenge.getNoOfTasks()+1);
        challengeRepository.save(challenge);

        return new ChallengeResponse(result);
    }

    public List<Challenge> getTop3Results() {
        return  challengeRepository.findAllSortedBySuccess(PageRequest.of(0,3));

    }

}
