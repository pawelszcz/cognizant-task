package com.cognizant.challenge;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ChallengeRequest {
    private String name;
    private String script;
    private Integer taskId;
    private String input;
}
